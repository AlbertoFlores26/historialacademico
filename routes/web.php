<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BuscarEstudianteController;
use App\Http\Controllers\PdfEstudianteController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

route::get('Estudiante', [BuscarEstudianteController::class, 'index']); 

route::get('Estudiante/informacion', [BuscarEstudianteController::class, 'show']);

route::get('Estudiante/informacion/pdf/vista', [PdfEstudianteController::class, 'index']);

route::get('Estudiante/informacion/pdf', [PdfEstudianteController::class, 'ImprimirPDF']);

