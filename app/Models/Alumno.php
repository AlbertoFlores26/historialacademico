<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    use HasFactory;
    protected $table = "estudiante";
    protected $fillable = [
        'idEstudiante',
        'ews_Matricula' ,
        'ews_Nombre' ,
        'ews_ApellidoPaterno' ,
        'ews_ApellidoMaterno' ,
        'ews_Sexo' ,
        'idEstadoCivil' ,
        'ews_Nacionalidad' ,
        'ews_LugarNaci' ,
        'ews_FechaNacimiento' ,
        'ews_CURP',
        'ews_RFC' ,
        'ews_IMSS' ,
        'ews_Email' ,
        'ews_Telefono',
        'ews_FechaCreacion',
        'idEstatus',
    ];
}
