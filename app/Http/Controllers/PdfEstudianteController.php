<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alumno;
use Illuminate\Support\Facades\DB;
use PDF;

class PdfEstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('PdfEstudiante');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ImprimirPDF()
    {
   
       /* $informacion = DB::table('calificacionalumno')
        ->select('estudiante.ews_ApellidoPaterno', 'estudiante.ews_ApellidoMaterno', 'estudiante.ews_Nombre', 'licenciatura.ews_Licenciatura', 'semestre.ews_Semestre', 'tipoasignatura.ews_Tipo', 'asignatura.ews_clave', 'asignatura.ews_Asignatura', 'calificacionalumno.ews_Creditos', 'calificacionalumno.ews_Calificacion')
        ->join('estudiante','calificacionalumno.idEstudiante','=','estudiante.idEstudiante')
        ->join('periodosemestre','calificacionalumno.idPeriodo','=','periodosemestre.idPeriodo')
        ->join('asignatura','asignatura.idAsignatura','=','calificacionalumno.idAsignatura')
        ->join('semestre','periodosemestre.idSemestre','=','semestre.idSemestre')
        ->join('tipoasignatura','asignatura.idTipoasignatura','=','tipoasignatura.idTipoasignatura')
        ->join('licenciaturaalumno','licenciaturaalumno.idEstudiante','=','estudiante.idEstudiante')
        ->join('licenciatura','licenciatura.idLicenciatura','=','licenciaturaalumno.idLicenciatura')
        ->where('estudiante.ews_Matricula','=','18391020')
        ->get(); */

        $pdf = PDF::loadView('PdfEstudiante');
        return $pdf->setPaper('a4', 'landscape')->stream('Historial.pdf');
    }
}
