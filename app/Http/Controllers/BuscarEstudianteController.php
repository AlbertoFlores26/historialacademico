<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alumno;
use Illuminate\Support\Facades\DB;

class BuscarEstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

            $Estudiante = DB::table('estudiante')
            ->select('estudiante.ews_Matricula', 'estudiante.ews_Nombre', 'estudiante.ews_ApellidoPaterno', 'estudiante.ews_ApellidoMaterno', 'estudiante.ews_Sexo', 'estudiante.idEstadoCivil', 'estudiante.ews_Nacionalidad', 'estudiante.ews_LugarNaci', 'estudiante.ews_FechaNacimiento', 'estudiante.ews_CURP', 'estudiante.ews_RFC', 'estudiante.ews_IMSS', 'estudiante.ews_Email', 'estudiante.ews_Telefono', 'estudiante.ews_FechaCreacion', 'departamento.ews_Descripcion', 'estatu.ews_Descripcion')
            ->join('departamentoestudiante','estudiante.idEstudiante','=','departamentoestudiante.idEstudiante')
            ->join('departamento','departamentoestudiante.idDepartamento','=','departamento.idDepartamento')
            ->join('estatu','estudiante.idEstatus','=','estatu.idEstatus')
            ->get();

            return $Estudiante;
            
     /*       return json_encode(array
                    (
                        200 => array (
                        'wsp_Estudiantes'  =>  $Estudiante)
                    )
            );    
    /*     
/*    foreach ($Estudiante as $key) {
                $json = array(
    
                    "wsp_status" =>200,
                    "wsp_total_registros" => count($Estudiante),
                    "wsp_mensaje" => $key->ews_Matricula
                );
                
            }
                return json_encode($json, true); 
                
                */
    
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $matricula = $request->input('ews_Matricula');

        $existencia = DB::table('estudiante')
        ->select('estudiante.ews_Matricula')
        ->where('estudiante.ews_Matricula','=',$matricula)
        ->get();

        // return $existencia;

        if ($existencia = $matricula) {   

            $Estudiante = DB::table('estudiante')
            ->select('estudiante.ews_Matricula', 'estudiante.ews_Nombre', 'estudiante.ews_ApellidoPaterno', 'estudiante.ews_ApellidoMaterno', 'estudiante.ews_Sexo', 'estudiante.idEstadoCivil', 'estudiante.ews_Nacionalidad', 'estudiante.ews_LugarNaci', 'estudiante.ews_FechaNacimiento', 'estudiante.ews_CURP', 'estudiante.ews_RFC', 'estudiante.ews_IMSS', 'estudiante.ews_Email', 'estudiante.ews_Telefono', 'estudiante.ews_FechaCreacion', 'departamento.ews_Descripcion', 'estatu.ews_Descripcion')
            ->join('departamentoestudiante','estudiante.idEstudiante','=','departamentoestudiante.idEstudiante')
            ->join('departamento','departamentoestudiante.idDepartamento','=','departamento.idDepartamento')
            ->join('estatu','estudiante.idEstatus','=','estatu.idEstatus')
            ->where('estudiante.ews_Matricula','=',$matricula)
            ->get();
           
            return $Estudiante;

        }else{

            return  json_encode( 
               array (
                    400 => array('wsp_mensaje' => 'No existe la matricula ingresada, revise con su institución')
                          )
                   );
            
        } 

        

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
