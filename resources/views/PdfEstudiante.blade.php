<style>
      @page {
            margin: 0cm 0cm;
            font-size: 1em;
        }

        body {
            margin: 3cm 2cm 2cm;
        }

      

        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
            background-color: #F93855;
            color: white;
            text-align: center;
            line-height: 35px;
        }
</style>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>TABLA DE PRODUCTOS</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<body>
	<header>
        <br>
        <p><strong>LIBRERIA DOMPDF - LARAVEL 7</strong></p>
    </header>
    <main>
        <h5 style="text-align: center"><strong>TABLA DE PRODUCTOS</strong></h5>
        <table class="table table-striped text-center">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Existencia</th>
                    <th scope="col">Lote</th>
                    <th scope="col">Fecha</th>
                </tr>
            </thead>
           <tbody>
                <tr>
                    <th scope="row">d</th>
                    <tdd>d</tdd>
                    <td>d</td>
                    <td>d</td>
                    <td>f</td>
                    <td>jh</td>
                </tr>
            </tbody>
        </table>
    </main>
    <footer>
        <p><strong>SUSCRIBETE - COMENTA - COMPARTE</strong></p>
    </footer>
</body>
</html>